# Generated by Django 4.2.1 on 2023-05-31 22:00

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0002_todoitem"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todoitem",
            old_name="list",
            new_name="todo_list",
        ),
    ]
