from django.contrib import admin
from .models import TodoList, TodoItem


@admin.register(TodoList)
class Todo_List_Admin(admin.ModelAdmin):
    list_display = ("name", "id")

    @admin.register(TodoItem)
    class Todo_Item_Admin(admin.ModelAdmin):
        list_display = ('task', 'due_date', 'is_completed',)


# Register your models here.clear
# admin.site.register(TodoList)
